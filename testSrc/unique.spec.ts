import { describe } from "@b08/test-runner";
import { unique } from "../src/unique";

describe("unique", it => {
  it("should return items unique by primitive key", expect => {
    // arrange
    const src = [{ a: "1", b: 2 }, { a: "2", b: 3 }, { a: "1", b: 4 }];
    const expected = [src[0], src[1]];
    // act
    const result = unique(src, item => item.a);

    // assert
    expect.deepEqual(result, expected);
  });

  it("should return items unique by flat key", expect => {
    // arrange
    const src = [{ a: "1", b: 2 }, { a: "1", b: 3 }, { a: "2", b: 4 }];
    const expected = [src[0], src[2]];
    // act
    const result = unique(src, item => ({ a: item.a }));

    // assert
    expect.deepEqual(result, expected);
  });

  it("should return unique flat keys", expect => {
    // arrange
    const src = [{ a: "1", b: 2 }, { a: "1", b: 3 }, { a: "1", b: 2 }, { a: "2", b: 4 }];
    const expected = [src[0], src[1], src[3]];
    // act
    const result = unique(src);

    // assert
    expect.deepEqual(result, expected);
  });
});
