import { KeyStore, getKeyStore } from "./keyStore";

export class KeyGroup<TKey, TVal> {
  private keyStore: KeyStore<TKey, TVal>;
  private valuesMap: Map<TKey, TVal[]> = new Map<TKey, TVal[]>();

  constructor(public keySelector: (val: TVal) => TKey = null, items: TVal[] = []) {
    this.keyStore = getKeyStore<TKey, TVal>(keySelector);
    items.forEach(item => this.addItem(item));
  }

  public values(): TVal[][] {
    return Array.from(this.valuesMap.values());
  }

  public keys(): TKey[] {
    return Array.from(this.valuesMap.keys());
  }

  public add = (key: TKey, item: TVal) => this.addElement(this.keyStore.getOrSetKey(key), item);
  public set: (key: TKey, item: TVal) => void = this.add;
  public addItem = (item: TVal) => this.add(this.keySelector(item), item);

  public get(key: TKey): TVal[] {
    const existing = this.keyStore.getKey(key);
    return existing != null ? this.valuesMap.get(existing) : null;
  }

  public has(key: TKey): boolean {
    const existing = this.keyStore.getKey(key);
    return existing != null && this.valuesMap.has(existing);
  }

  public hasItem(item: TVal): boolean {
    const items = this.get(this.keySelector(item));
    return Array.isArray(items) && items.indexOf(item) !== -1;
  }

  public delete(key: TKey): void {
    key = this.keyStore.deleteKey(key);
  }

  public deleteItem(item: TVal): boolean {
    const key = this.keySelector(item);
    const items = this.get(key);
    if (items == null) {
      return false;
    }
    const filtered = items.filter(i => i !== item);
    if (filtered.length === 0) {
      this.delete(key);
    }
    return filtered.length < items.length;
  }

  private addElement(key: TKey, item: TVal): void {
    let items = this.get(key);
    if (items == null) {
      this.valuesMap.set(key, [item]);
    } else {
      items.push(item);
    }
  }
}
