import { KeyStore, getKeyStore } from "./keyStore";

export class KeySet<TKey> {
  private keyStore: KeyStore<TKey, TKey>;
  private val: Set<TKey> = new Set();

  constructor(items: TKey[] = []) {
    this.keyStore = getKeyStore<TKey, TKey>(null);
    items.forEach(item => this.getOrAdd(item));
  }

  public values(): TKey[] {
    return Array.from(this.val.values());
  }

  public getOrAdd(key: TKey): TKey {
    const newKey = this.keyStore.getOrSetKey(key);
    if (newKey === key) { this.val.add(key); }
    return newKey;
  }

  public has(key: TKey): boolean {
    return this.keyStore.getKey(key) != null;
  }

  public delete(key: TKey): void {
    this.val.delete(this.keyStore.deleteKey(key));
  }
}
