import { describe } from "@b08/test-runner";
import { mapBy } from "../src";

describe("mapBy", it => {
  it("should return mapped entities", expect => {
    // arrange
    const src = [
      { f1: 1, f2: 2, f3: 1 },
      { f1: 2, f2: 1, f3: 2 },
      { f1: 1, f2: 2, f3: 3 },
      { f1: 2, f2: 1, f3: 4 },
      { f1: 1, f2: 2, f3: 5 },
    ];
    const keySelector = item => ({ f1: item.f1, f2: item.f2 });

    // act
    const map = mapBy(src, keySelector);

    // assert
    const val = map.values();
    expect.equal(val.length, 2);
    expect.deepEqual(val[0], src[4]);
    expect.deepEqual(val[1], src[3]);
    expect.true(map.has(keySelector(src[0])));
  });

  it("should work with value key as well", expect => {
    // arrange
    const src = [
      { f1: 2, f2: 1, f3: 4 },
      { f1: 1, f2: 2, f3: 5 },
    ];

    // act
    const map = mapBy(src, item => item.f1);

    // assert
    const val = map.values();
    expect.equal(val.length, 2);
    expect.deepEqual(val[0], src[0]);
    expect.deepEqual(val[1], src[1]);
  });

  it("should get by key value", expect => {
    // arrange
    const src = [
      { f1: 2, f2: 1, f3: 4 },
      { f1: 1, f2: 2, f3: 5 },
    ];

    // act
    const map = mapBy(src, item => item.f1);
    const result = map.get(1);

    // assert
    expect.deepEqual(result, src[1]);
  });

  it("should map one field by another", expect => {
    // arrange
    const src = [
      { f1: 2, f2: 1 },
      { f1: 1, f2: 2 },
    ];

    // act
    const map = mapBy(src, item => item.f1, item => item.f2);

    // assert
    expect.equal(map.get(1), 2);
    expect.equal(map.get(2), 1);
  });
});
