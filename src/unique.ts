import { KeySet } from "./keySet";
import { KeyMap } from "./keyMap";

export function unique<TVal, TKey = TVal>(arr: TVal[], selector?: (val: TVal) => TKey): TVal[] {
  if (!Array.isArray(arr)) { return null; }
  if (selector == null) { return new KeySet(arr).values(); }
  const map = new KeyMap(selector);
  return arr.filter(item => {
    const key = selector(item);
    if (map.has(key)) { return false; }
    map.add(key, item);
    return true;
  });
}
