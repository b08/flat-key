import { KeyStore, getKeyStore } from "./keyStore";

export class KeyMap<TKey, TVal> {
  private keyStore: KeyStore<TKey, TVal>;
  private valuesMap: Map<TKey, TVal> = new Map<TKey, TVal>();

  constructor(public keySelector: (val: TVal) => TKey = null, items: TVal[] = []) {
    this.keyStore = getKeyStore(keySelector);
    items.forEach(item => this.addItem(item));
  }

  public values(): TVal[] {
    return Array.from(this.valuesMap.values());
  }

  public keys(): TKey[] {
    return Array.from(this.valuesMap.keys());
  }

  public get(key: TKey): TVal {
    const existing = this.keyStore.getKey(key);
    return existing != null ? this.valuesMap.get(existing) : null;
  }

  public add(key: TKey, item: TVal): void {
    key = this.keyStore.getOrSetKey(key);
    this.valuesMap.set(key, item);
  }

  public set: (key: TKey, item: TVal) => void = this.add;

  public addItem(item: TVal): void {
    this.add(this.keySelector(item), item);
  }

  public delete(key: TKey): void {
    key = this.keyStore.deleteKey(key);
    this.valuesMap.delete(key);
  }

  public deleteItem(item: TVal): boolean {
    const key = this.keySelector(item);
    const existing = this.get(key);
    if (existing === item) {
      this.delete(key);
      return true;
    }
    return false;
  }

  public has(key: TKey): boolean {
    const existing = this.keyStore.getKey(key);
    return existing != null && this.valuesMap.has(existing);
  }

  public hasItem(item: TVal): boolean {
    return item === this.get(this.keySelector(item));
  }
}
