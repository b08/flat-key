import { describe } from "@b08/test-runner";
import { KeySet } from "../src";

describe("keySet", it => {
  it("should not add existing key", expect => {
    // arrange
    const src = [{ a: "1", b: 2 }, { a: "1", b: 2 }, { a: "1", b: 3 }];

    // act
    const target = new KeySet(src);
    const values = target.values();

    // assert
    expect.equal(values.length, 2);
    expect.equal(values[0], src[0]);
    expect.equal(values[1], src[2]);
  });

  it("should not add same key", expect => {
    // arrange
    const key = { a: "1", b: 2 };
    const src = [key, { a: "1", b: 3 }, key];

    // act
    const target = new KeySet(src);
    const values = target.values();

    // assert
    expect.equal(values.length, 2);
    expect.equal(values[0], src[0]);
    expect.equal(values[1], src[1]);
  });
});
