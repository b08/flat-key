import { describe } from "@b08/test-runner";
import { groupBy } from "../src";

type Test = { f1: number, f2: number, f3: number };

describe("groupBy", it => {
  it("should return grouped entities", expect => {
    // arrange

    const src: Test[] = [
      { f1: 1, f2: 2, f3: 1 },
      { f1: 2, f2: 1, f3: 2 },
      { f1: 1, f2: 2, f3: 3 },
      { f1: 2, f2: 1, f3: 4 },
      { f1: 1, f2: 2, f3: 5 },
    ];

    // act
    const group = groupBy(src, item => ({ f1: item.f1, f2: item.f2 }));

    // assert
    const val = group.values();
    expect.equal(val.length, 2);
    expect.deepEqual(val[0], [src[0], src[2], src[4]]);
    expect.deepEqual(val[1], [src[1], src[3]]);
  });

  it("should work with value key as well", expect => {
    // arrange
    const src: Test[] = [
      { f1: 1, f2: 2, f3: 1 },
      { f1: 2, f2: 1, f3: 2 },
      { f1: 1, f2: 2, f3: 3 },
      { f1: 2, f2: 1, f3: 4 },
      { f1: 1, f2: 2, f3: 5 },
    ];

    // act
    const group = groupBy(src, item => item.f1);

    // assert
    const val = group.values();
    expect.equal(val.length, 2);
    expect.deepEqual(val[0], [src[0], src[2], src[4]]);
    expect.deepEqual(val[1], [src[1], src[3]]);
  });

  it("should get by value key", expect => {
    // arrange
    const src: Test[] = [
      { f1: 1, f2: 2, f3: 1 },
      { f1: 2, f2: 1, f3: 2 },
      { f1: 1, f2: 2, f3: 3 },
      { f1: 2, f2: 1, f3: 4 },
      { f1: 1, f2: 2, f3: 5 },
    ];

    // act
    const group = groupBy(src, item => item.f1);
    const result = group.get(2);

    // assert
    expect.deepEqual(result, [src[1], src[3]]);
  });

  it("should group one field by another", expect => {
    // arrange
    const src = [
      { f1: 1, f2: 1 },
      { f1: 2, f2: 2 },
      { f1: 1, f2: 3 },
      { f1: 2, f2: 4 },
      { f1: 1, f2: 5 },
    ];

    // act
    const group = groupBy(src, item => item.f1, item => item.f2);

    // assert
    expect.deepEqual(group.get(1), [1, 3, 5]);
    expect.deepEqual(group.get(2), [2, 4]);
  });
});
