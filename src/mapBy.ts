import { KeyMap } from "./keyMap";

export function mapBy<TK, TV>(arr: TV[], s1: (v: TV) => TK): KeyMap<TK, TV>;
export function mapBy<TS, TK, TV>(arr: TS[], s1: (s: TS) => TK, s2: (s: TS) => TV): KeyMap<TK, TV>;
export function mapBy<TS, TK, TV>(arr: TS[], s1: (s: TS) => TK, s2?: (s: TS) => TV): KeyMap<TK, TV> {
  if (s2 == null) { return <any>new KeyMap(s1, arr); }
  const group = new KeyMap<TK, TV>();
  if (Array.isArray(arr)) { arr.forEach(i => group.add(s1(i), s2(i))); }
  return group;
}
