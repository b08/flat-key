export class KeyStore<TKey, TVal> {
  private fields: string[];
  private lastField: string;
  private keysMap: Map<any, any> = new Map();

  constructor(protected keySelector: (val: TVal) => TKey) { }

  public getOrSetKey(key: TKey): TKey {
    this.actOnKeyType(key);
    return this.getOrSetKey(key);
  }

  public getKey(key: TKey): TKey {
    this.actOnKeyType(key);
    return this.getKey(key);
  }

  public deleteKey(key: TKey): TKey {
    this.actOnKeyType(key);
    return this.deleteKey(key);
  }

  private actOnKeyType(key: TKey): void {
    // this basic check is here for basic libraries to not depend on each other
    if (key == null || typeof (key) !== "object") {
      this.getOrSetKey = this.getKey = this.deleteKey = key => key;
      return;
    }
    this.fillFieldNames(key);
    this.getKey = this.multiGetKey;
    this.getOrSetKey = this.multiGetOrSetKey;
    this.deleteKey = this.multiDeleteKey;
  }

  private multiGetKey = (key: TKey) => {
    if (key == null) { throw new Error("Key cannot be null"); }
    const lastMap = this.fields.reduce((map, field) => map && map.get(key[field]), this.keysMap);
    return lastMap && lastMap.get(key[this.lastField]);
  }

  private multiGetOrSetKey = (key: TKey) => {
    if (key == null) { throw new Error("Key cannot be null"); }
    const lastMap = this.fields.reduce((map, field) => {
      const val = key[field];
      let subMap = map.get(val);
      if (subMap == null) {
        subMap = new Map();
        map.set(val, subMap);
      }
      return subMap;
    }, this.keysMap);

    const lastVal = key[this.lastField];
    if (lastMap.has(lastVal)) {
      return lastMap.get(lastVal);
    } else {
      lastMap.set(lastVal, key);
      return key;
    }
  }

  private multiDeleteKey = (key: TKey) => {
    if (key == null) { throw new Error("Key cannot be null"); }
    return this.deleteFromMap(this.keysMap, [...this.fields, this.lastField], key);
  }

  private deleteFromMap = (map: Map<any, any>, fields: string[], key: TKey) => {
    const fieldKey = key[fields[0]];
    if (fields.length === 1) {
      const val = map.get(fieldKey);
      map.delete(fieldKey);
      return val;
    }

    const childMap: Map<any, any> = map.get(fieldKey);
    if (childMap === null) { return key; }
    var val = this.deleteFromMap(childMap, fields.slice(1), key);
    if (childMap.size === 0) { map.delete(fieldKey); }
    return val;
  }

  private fillFieldNames(key: any): void {
    const fields = Object.keys(key);
    this.fields = fields.slice(0, fields.length - 1);
    this.lastField = fields[fields.length - 1];
  }
}
