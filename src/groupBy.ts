import { KeyGroup } from "./keyGroup";

export function groupBy<TK, TV>(arr: TV[], selector: (val: TV) => TK): KeyGroup<TK, TV>;
export function groupBy<TS, TK, TV>(arr: TS[], s1: (s: TS) => TK, s2: (s: TS) => TV): KeyGroup<TK, TV>;
export function groupBy<TS, TK, TV>(arr: TS[], s1: (s: TS) => TK, s2?: (s: TS) => TV): KeyGroup<TK, TV> {
  if (s2 == null) { return <any>new KeyGroup(s1, arr); }
  const group = new KeyGroup<TK, TV>();
  if (Array.isArray(arr)) { arr.forEach(i => group.add(s1(i), s2(i))); }
  return group;
}
