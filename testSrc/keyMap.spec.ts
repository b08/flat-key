import { KeyMap } from "../src";
import { describe } from "@b08/test-runner";

describe("keyMap", it => {
  it("should add with key", expect => {
    // arrange
    const value = {};
    const key = { a: "1", b: 2 };
    const target = new KeyMap();

    // act
    target.add(key, value);
    const result = target.get(key);

    // assert
    expect.equal(result, value);
  });

  it("should delete item", expect => {
    // arrange
    const value = {}, value2 = {};
    const key = { a: "1", b: 2 };
    const target = new KeyMap();
    target.add(key, value);

    // act
    target.delete(key);
    const has = target.has(key);
    target.add(key, value2);
    const result = target.get(key);

    // assert
    expect.false(has);
    expect.equal(result, value2);
  });

  it("should work with 4 field key", expect => {
    // arrange

    const key1 = { 0: "1", 1: "2", 2: "2", 3: "3" };
    const key2 = { 0: "1", 1: "4", 2: "2", 3: "3" };
    const target = new KeyMap();

    // act
    target.add(key1, 1);
    target.add(key2, 2);

    // assert
    expect.equal(target.get(key1), 1);
    expect.equal(target.get(key2), 2);
  });
});
